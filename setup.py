from setuptools import setup, find_packages

__version__ = "1.0.0"

setup(name="mongo_client",
      version=__version__,
      packages=find_packages(),
      python_requires=">=3.7")