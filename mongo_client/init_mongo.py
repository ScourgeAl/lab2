def init(coll):
    init_db = \
    {
        "student1": {
            "name": "Ivan",
            "surname": "Ivanov",
            "birth_date": "1998/03/15",
            "gradebook": 
            {
                "subj1":
                {
                    "name": "math",
                    "exam_date": "2022/06/10",
                    "teacher": "Tapochkin Sergey Nikolaevich",
                    "grade": 5
                },
                "subj2":
                {
                    "name": "cs",
                    "exam_date": "2022/06/14",
                    "teacher": "Malikov Vladimir Sergeevich",
                    "grade": 3
                },
                "subj3":
                {
                    "name": "english",
                    "exam_date": "2022/06/21",
                    "teacher": "Gladkova Nina Nikolaevna",
                    "grade": 4
                }
            }
        },
        "student2":{
            "name": "Dmitriy",
            "surname": "Kostin",
            "birth_date": "1999/01/16",
            "gradebook": 
            {
                "subj1":
                {
                    "name": "math",
                    "exam_date": "2022/06/16",
                    "teacher": "Tapochkin Sergey Nikolaevich",
                    "grade": 4
                },
                "subj2":
                {
                    "name": "cs",
                    "exam_date": "2022/06/11",
                    "teacher": "Malikov Vladimir Sergeevich",
                    "grade": 5
                },
                "subj3":
                {
                    "name": "english",
                    "exam_date": "2022/06/26",
                    "teacher": "Gladkova Nina Nikolaevna",
                    "grade": 5
                }
            }
        },
        "student3":{
            "name": "Natalia",
            "surname": "Drozdova",
            "birth_date": "1997/09/29",
            "gradebook": 
            {
                "subj1":
                {
                    "name": "math",
                    "exam_date": "2022/06/12",
                    "teacher": "Tapochkin Sergey Nikolaevich",
                    "grade": 5
                },
                "subj2":
                {
                    "name": "cs",
                    "exam_date": "2022/06/18",
                    "teacher": "Malikov Vladimir Sergeevich",
                    "grade": 5
                },
                "subj3":
                {
                    "name": "english",
                    "exam_date": "2022/06/25",
                    "teacher": "Gladkova Nina Nikolaevna",
                    "grade": 5
                }
            }
        },
        "student4":{
            "name": "Sofia",
            "surname": "Gluhova",
            "birth_date": "1999/02/05",
            "gradebook": 
            {
                "subj1":
                {
                    "name": "math",
                    "exam_date": "2022/06/12",
                    "teacher": "Tapochkin Sergey Nikolaevich",
                    "grade": 3
                },
                "subj2":
                {
                    "name": "cs",
                    "exam_date": "2022/06/18",
                    "teacher": "Malikov Vladimir Sergeevich",
                    "grade": 4
                },
                "subj3":
                {
                    "name": "english",
                    "exam_date": "2022/06/25",
                    "teacher": "Gladkova Nina Nikolaevna",
                    "grade": 5
                }
            }
        },
        "student5":{
            "name": "Kim",
            "surname": "Polykov",
            "birth_date": "1998/07/21",
            "gradebook": 
            {
                "subj1":
                {
                    "name": "math",
                    "exam_date": "2022/06/19",
                    "teacher": "Tapochkin Sergey Nikolaevich",
                    "grade": 3
                },
                "subj2":
                {
                    "name": "cs",
                    "exam_date": "2022/06/14",
                    "teacher": "Malikov Vladimir Sergeevich",
                    "grade": 3
                },
                "subj3":
                {
                    "name": "english",
                    "exam_date": "2022/06/24",
                    "teacher": "Gladkova Nina Nikolaevna",
                    "grade": 3
                }
            }
        }
    }

    coll.delete_many({})

    coll.insert_many([{_k: _v for _k, _v in v.items()} for _, v in init_db.items()])