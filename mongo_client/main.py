import os
from pymongo import MongoClient
from init_mongo import init
from query import show_exams, show_math, show_eng, show_cs

OUTPUT_PATH = "output"

client = MongoClient(
        host = ["mongo:27017"],
        serverSelectionTimeoutMS = 6000,
        username = "root",
        password = "example",
    )

students_collection = client.admin.students

init(students_collection)

# result = show_exams(students_collection)
res1 = show_math(students_collection)
res2 = show_cs(students_collection)
res3 = show_eng(students_collection)

print("Exam dates are:")

# os.makedirs(OUTPUT_PATH, exist_ok=True)
# with open(os.path.join(OUTPUT_PATH, "math.txt"), "w+") as file:
#     for doc in res1:
#         print(doc)
#         file.write(str(doc))
#         file.write("\n")

# os.makedirs(OUTPUT_PATH, exist_ok=True)
# with open(os.path.join(OUTPUT_PATH, "cs.txt"), "w+") as file:
#     for doc in res2:
#         print(doc)
#         file.write(str(doc))
#         file.write("\n")

# os.makedirs(OUTPUT_PATH, exist_ok=True)
# with open(os.path.join(OUTPUT_PATH, "eng.txt"), "w+") as file:
#     for doc in res3:
#         print(doc)
#         file.write(str(doc))
#         file.write("\n")

os.makedirs(OUTPUT_PATH, exist_ok=True)
with open(os.path.join(OUTPUT_PATH, "exams.txt"), "w+") as file:
    for doc in res1:
        print(doc)
        file.write(str(doc))
        file.write("\n")
    for doc in res2:
        print(doc)
        file.write(str(doc))
        file.write("\n")
    for doc in res3:
        print(doc)
        file.write(str(doc))
        file.write("\n")

# with open(os.path.join(OUTPUT_PATH, "res.txt"), "w+") as file:
#     for doc in result:
#         print(doc)
#         file.write(str(doc))
#         file.write("\n")